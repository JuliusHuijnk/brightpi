# README #

Render text on a 3x3 led matrix on your Raspberry Pi.

[See the code in action](https://youtu.be/GIoXjr58790)

## How do I get set up? ##

### Set up the 3x3 led matrix ###

* Follow along with these video tutorials by Gaven MacDonald 'Raspberry Pi Project: The LED Matrix'
https://www.youtube.com/playlist?list=PLhpG9Bht0q6m7E4nD12eeA-xMFUKfvS-8
* I didn't solder but used a bread pin board, contact me if you need help with that.
* I used a Raspberry Pi B+, and I didn't test it on other versions. Make sure you set your pins right, but apart from that nothing fancy is going on that is specific to the B+.

### Check your GIO pin configuration ###

I used  4, 17, 27 for anodes and 23, 24, 25 for cathodes. Make sure you use the same pins, or change the file before your run it.

### Run present_text('hello world') ###

At the end of the file you can see the present_text function. Change the variables and have fun!

## Contribution guidelines ###

Feel free to contact me if you want to add something or have questions.